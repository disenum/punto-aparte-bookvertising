<?php
$noticias = array(
    [
        "name" => "Titular de noticia 1",
        "date" => "2017/04/01",
        "decription" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "folder" => "Ministerio de Educacion_Colombia, la mas educada",
        "img_path" => "PA_2017-02_LA-MEN_01.jpg",
    ],
    [
        "name" => "Titular de noticia 2",
        "date" => "2017/04/01",
        "decription" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "folder" => "Ministerio de Educacion_Colombia, la mas educada",
        "img_path" => "PA_2017-02_LA-MEN_02.jpg",
    ],
    [
        "name" => "Titular de noticia 3",
        "date" => "2017/04/01",
        "decription" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "folder" => "Ministerio de Educacion_Colombia, la mas educada",
        "img_path" => "PA_2017-02_LA-MEN_04.jpg",
    ],
    [
        "name" => "Titular de noticia 4",
        "date" => "2017/04/01",
        "decription" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "folder" => "Ministerio de Educacion_Colombia, la mas educada",
        "img_path" => "PA_2017-02_LA-MEN_03.jpg",
    ],
    [
        "name" => "Titular de noticia 5",
        "date" => "2017/04/01",
        "decription" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "folder" => "Ministerio de Educacion_Colombia, la mas educada",
        "img_path" => "PA_2017-02_LA-MEN_05.jpg",
    ],
    [
        "name" => "Titular de noticia 6",
        "date" => "2017/04/01",
        "decription" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "folder" => "Ministerio de Educacion_Colombia, la mas educada",
        "img_path" => "PA_2017-02_LA-MEN_06.jpg",
    ],
);  