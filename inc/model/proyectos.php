<?php
$proyectos = array(
    [
        "name" => "Colombia, la mejor educada 2025",
        "client" => "Ministerio de Educación Nacional",
        "decription" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "folder" => "Ministerio de Educacion_Colombia, la mas educada",
        "img_path" => "PA_2017-02_LA-MEN_01.jpg",
        "images" =>  [ 
            'PA_2017-02_LA-MEN_01.jpg',
            'PA_2017-02_LA-MEN_02.jpg',
            'PA_2017-02_LA-MEN_03.jpg',
            'PA_2017-02_LA-MEN_04.jpg',
            'PA_2017-02_LA-MEN_05.jpg',
            'PA_2017-02_LA-MEN_06.jpg',
            'PA_2017-02_LA-MEN_07.jpg',
            'PA_2017-02_LA-MEN_08.jpg',
            'PA_2017-02_LA-MEN_09.jpg',
            'PA_2017-02_LA-MEN_10.jpg'
        ]
    ],
    array(
        "name" => "Orinoquia Viva",
        "client" => "Humboldt_Orinoquia",
        "decription" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "folder" => "Instituto-Humboldt_Orinoquia-Viva",
        "img_path" => "PA_2017-02_LA-Orinoquia_01.jpg",
        "images" => [
            'PA_2017-02_LA-Orinoquia_01.jpg',
            'PA_2017-02_LA-Orinoquia_02.jpg',
            'PA_2017-02_LA-Orinoquia_03.jpg',
            'PA_2017-02_LA-Orinoquia_04.jpg',
            'PA_2017-02_LA-Orinoquia_05.jpg',
            'PA_2017-02_LA-Orinoquia_06.jpg',
            'PA_2017-02_LA-Orinoquia_07.jpg',
            'PA_2017-02_LA-Orinoquia_08.jpg',
            'PA_2017-02_LA-Orinoquia_09.jpg',
            'PA_2017-02_LA-Orinoquia_10.jpg'
        ]
    ),
    array(
        "name" => "BIO 2016",
        "client" => "Instituto-Humboldt_Orinoquia-Viva",
        "decription" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "folder" => "Instituto-Humboldt_Biodiversidad-2015",
        "img_path" => "PA_2017-02_LA-BIO_01.jpg",
        "images" => [
            'PA_2017-02_LA-BIO_01.jpg',
            'PA_2017-02_LA-BIO_02.jpg',
            'PA_2017-02_LA-BIO_03.jpg',
            'PA_2017-02_LA-BIO_04.jpg',
            'PA_2017-02_LA-BIO_05.jpg',
            'PA_2017-02_LA-BIO_06.jpg',
            'PA_2017-02_LA-BIO_07.jpg',
            'PA_2017-02_LA-BIO_08.jpg',
            'PA_2017-02_LA-BIO_09.jpg',
            'PA_2017-02_LA-BIO_10.jpg'
        ]
    ),
    array(
        "name" => "Colombia Anfibia",
        "client" => "Instituto-Humboldt_Orinoquia-Viva",
        "decription" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "folder" => "Instituto-Humboldt_Colombia-Anfibia",
        "img_path" => "PA_2017-02_LA-ColAnfibia08.jpg",
        "images" => [
            'PA_2017-02_LA-ColAnfibia08.jpg',
            'PA_2017-02_LA-ColAnfibia09.jpg',
            'PA_2017-02_LA-ColAnfibia10.jpg',
            'PA_2017-02_LA-ColAnfibia2_01.jpg',
            'PA_2017-02_LA-ColAnfibia2_02.jpg',
            'PA_2017-02_LA-ColAnfibia2_03.jpg',
            'PA_2017-02_LA-ColAnfibia2_04.jpg',
            'PA_2017-02_LA-ColAnfibia2_05.jpg',
            'PA_2017-02_LA-ColAnfibia2_06.jpg',
            'PA_2017-02_LA-ColAnfibia2_07.jpg'
        ]
    ),
    array(
        "name" => "Naturaleza Urbana",
        "client" => "Instituto-Humboldt_Orinoquia-Viva",
        "decription" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "folder" => "Instituto-Humboldt_Naturaleza-Urbana",
        "img_path" => "PA_2017-02_LA-Urbana_01.jpg",
        "images" => [
            'PA_2017-02_LA-Urbana_01.jpg',
            'PA_2017-02_LA-Urbana_02.jpg',
            'PA_2017-02_LA-Urbana_03.jpg',
            'PA_2017-02_LA-Urbana_04.jpg',
            'PA_2017-02_LA-Urbana_05.jpg',
            'PA_2017-02_LA-Urbana_06.jpg',
            'PA_2017-02_LA-Urbana_07.jpg',
            'PA_2017-02_LA-Urbana_08.jpg',
            'PA_2017-02_LA-Urbana_09.jpg',
            'PA_2017-02_LA-Urbana_10.jpg'
        ]
    ),
    array(
        "name" => "Conocer, el primer paso para adaptarse",
        "client" => "IDEAM",
        "decription" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "folder" => "IDEAM_Conocer",
        "img_path" => "PA_2017-02_LA-Conocer_01.jpg",
        "images" => [
            'PA_2017-02_LA-Conocer_01.jpg',
            'PA_2017-02_LA-Conocer_02.jpg',
            'PA_2017-02_LA-Conocer_03.jpg',
            'PA_2017-02_LA-Conocer_04.jpg',
            'PA_2017-02_LA-Conocer_05.jpg',
            'PA_2017-02_LA-Conocer_06.jpg',
            'PA_2017-02_LA-Conocer_07.jpg',
            'PA_2017-02_LA-Conocer_08.jpg',
            'PA_2017-02_LA-Conocer_09.jpg',
            'PA_2017-02_LA-Conocer_10.jpg'
        ]
    ),
    array(
        "name" => "8 ciudades",
        "client" => "SCOPE",
        "decription" => "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "folder" => "SCOPE_8-ciudades",
        "img_path" => "PA_2017-02_LA-SCOPE_01.jpg",
        "images" => [
            'PA_2017-02_LA-SCOPE_01.jpg',
            'PA_2017-02_LA-SCOPE_02.jpg',
            'PA_2017-02_LA-SCOPE_03.jpg',
            'PA_2017-02_LA-SCOPE_04.jpg',
            'PA_2017-02_LA-SCOPE_05.jpg',
            'PA_2017-02_LA-SCOPE_06.jpg',
            'PA_2017-02_LA-SCOPE_07.jpg',
            'PA_2017-02_LA-SCOPE_08.jpg',
            'PA_2017-02_LA-SCOPE_09.jpg',
            'PA_2017-02_LA-SCOPE_10.jpg'
        ]
    )
);  