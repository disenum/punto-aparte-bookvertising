	<section class="content content-page">
      <div class="container">
        <div class="row">
        <?php foreach ($proyectos as $key => $proyecto) { ?>
          <div class="col-md-4 col-sm-6 grid-gallery">
          	<a href="<?= $base_url ?>proyectos/<?= $key+1 ?>">
	            <div class="thumbnail grid-gallery-entry">
	              <img class="img-responsive" style="width: 100%;" src="<?= $base_url ?>imgs/recursos/<?= $proyecto["folder"]."/".$proyecto["img_path"] ?>" />
	              <div class="noticia desc">
	                <p class="desc_tittle"><?= $proyecto["name"] ?></p>
	                <p class="desc_content"><?= $proyecto["client"] ?></p>
	              </div>
	            </div>
          	</a>
          </div>
		<?php } ?>
        </div>
      </div>
    </section>