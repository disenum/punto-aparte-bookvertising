<?php
	$proyecto = $proyectos[intval($subpageName) - 1 ]
?>
	<section class="content content-video">
      <div class="container">
        <div class="row">
        <?php foreach ($proyecto["images"] as $key => $image) { ?>
          <div class="col-sm-12">
              <img class="img-proyecto img-responsive" src="<?= $base_url ?>imgs/recursos/<?= $proyecto["folder"]."/".$image ?>" />
          </div>
		    <?php } ?>
        </div>
        <div class="row">
          <div class="proyecto-interno">
            <h2><?= $proyecto['name'] ?></h2>
            <p class="proyecto-interno-cliente"><?= $proyecto['client'] ?></p>
            <div class="proyecto-interno-descripcion">
              <p><?= $proyecto['decription'] ?></p>
            </div>
            <div class="back-element">
              <a href="<?= $base_url.'proyectos' ?>"><p><i class="fa fa-arrow-left"></i> Proyectos</p></a>
            </div>
          </div>
        </div>
      </div>
    </section>