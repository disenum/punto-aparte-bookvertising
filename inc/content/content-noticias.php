	<section class="content">
      <div class="container">
        <div class="row">
        <?php foreach ($noticias as $key => $noticia) { ?>
          <div class="col-md-4 col-sm-6 grid-gallery">
          	<a href="<?= $base_url ?>noticias/<?= $key+1 ?>">
	            <div class="thumbnail grid-gallery-entry">
	              <img class="img-responsive" style="width: 100%;" src="<?= $base_url ?>imgs/recursos/<?= $noticia["folder"]."/".$noticia["img_path"] ?>" />
	              <div class="noticia desc">
	                <p class="desc_tittle"><?= $noticia["name"] ?></p>
	                <p class="desc_content"><?= $noticia["date"] ?></p>
	              </div>
	            </div>
          	</a>
          </div>
		    <?php } ?>
        </div>
      </div>
    </section>