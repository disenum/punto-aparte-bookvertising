<?php
	$noticia = $noticias[intval($subpageName) - 1 ]
?>
	<section class="content content-video">
      <div class="container">
        <div class="row">
          <div class="col-sm-12">
              <img class="img-proyecto img-responsive" src="<?= $base_url ?>imgs/proyectos/<?= $noticia["folder"]."/".$noticia["img_path"] ?>" />
          </div>
        </div>
        <div class="row">
          <div class="proyecto-interno">
            <h2><?= $noticia['name'] ?></h2>
            <p class="proyecto-interno-cliente"><?= $noticia['date'] ?></p>
            <div class="proyecto-interno-descripcion">
              <p><?= $noticia['decription'] ?></p>
            </div>
            <div class="back-element">
              <a href="<?= $base_url.'noticias' ?>"><p><i class="fa fa-arrow-left"></i> Noticias</p></a>
            </div>
          </div>
        </div>
      </div>
    </section>