	<section class="content-contacto">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
		            <h2>Contacto</h2>
		            
		            <div class="contacto-info">
			            <div class="contacto-entry">
			            	<div class="contacto-icon">
	                    		<a href="mailto:contacto@puntoaparte.com.co"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>	
			            	</div>		            	
	                    	<a href="mailto:contacto@puntoaparte.com.co">contacto@puntoaparte.com.co</a>
			            </div>
			            <div class="contacto-entry">
			            	<div class="contacto-icon">
	                    		<a href="tel:+5717455952"> <i class="fa fa-phone fa-rotate-270" aria-hidden="true"></i></a>
	                		</div>	
	                    	<a href="tel:+5717455952">+57 1 745 5952</a>
	            		</div>	
	            		<div class="contacto-entry">
			            	<div class="contacto-icon">
	                    		<a href=""><i class="fa fa-map-marker" aria-hidden="true"></i></a>
	                		</div>	
	                    	<a href="#">Carrera 9 #71-38, oficina 605 Bogotá, Colombia</a>
	            		</div>	
		            </div>
				</div>
			</div>
		</div>
	</section>
