	   <footer>
      <section class="content">
        <div class=container>
          <div class="row">
            <div class="col-sm-4 footer-ubicacion">
              <p class="text-center">Bogotá, Colombia</p>
            </div>
            <div class="col-sm-4 centered footer-ubicacion">
              <p class="text-center">Copyrigth 2017 Punto Aparte</p>
            </div>
            <div class="col-sm-4 centered">
              <div class="footer-social">
                <ul>
                  <li><a href="http://facebook.com/"><i class="fa fa-facebook"></i></a></li>
                  <li><a href="http://twitter.com/"><i class="fa fa-twitter"></i></a></li>
                  <li><a href="http://pinterest.com/"><i class="fa fa-pinterest"></i></a></li>
                  <li><a href="http://behance.com/"><i class="fa fa-behance"></i></a></li>
                  <li><a href="http://instagram.com/"><i class="fa fa-instagram"></i></a></li>
                  <li><a href="http://youtube.com/"><i class="fa fa-youtube"></i> </a></li>
                </ul>
              </div>
          </div>
        </div>
      </section>
    </footer>


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script type="text/javascript" src="<?= $base_url ?>js/bootstrap.min.js"></script>
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script type="text/javascript" src="<?= $base_url ?>js/ie10-viewport-bug-workaround.js"></script>
    <script type="text/javascript" src="<?= $base_url ?>js/jquery.smooth-scroll.js"></script>
    <script type="text/javascript" src="<?= $base_url ?>js/mail.js"></script>
    <script type="text/javascript">
      $.smoothScroll('options',{speed:1000})
      $('.nav-link').smoothScroll();

      var amountScrolled = 300;

      $(window).scroll(function() {
        if ( $(window).scrollTop() > amountScrolled ) {
          $('a.back-to-top').fadeIn('slow');
        } else {
          $('a.back-to-top').fadeOut('slow');
        }
      });

      $('a.back-to-top').click(function() {
      $('html, body').animate({
          scrollTop: 0
        }, 700);
        return false;
      });

      $(function() {
          $("#img-descarga")
              .mouseover(function() {
                console.log('Hover'); 
                  var src = $(this).attr("src").match(/[^\.]+/) + "_hover.png";
                  $(this).attr("src", src);
              })
              .mouseout(function() {
                  var src = $(this).attr("src").replace("_hover.png", ".png");
                  $(this).attr("src", src);
              });
      });
    </script>
  </body>
</html>
