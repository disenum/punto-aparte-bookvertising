<!DOCTYPE html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <meta name="description" content="Punto Aparte.">
    <meta name="author" content="Nutshell Studio">
    <link rel="icon" href="<?= $base_url ?>imgs/favicon.ico">

    <meta property="og:url"           content="http://puntoaparte.com/" />
    <meta property="og:type"          content="website" />
    <meta property="og:title"         content="Punto Aparte" />
    <meta property="og:description"   content="Punto Aparte." />
    <meta property="og:image"         content="http://fintechgracion.com/imgs/fintechgracion-compartir-redes.png" />

    <title>Puntoaparte</title>

    <!-- Bootstrap core CSS -->
    <link href="<?= $base_url ?>css/bootstrap.min.css" rel="stylesheet">

    <!-- Dont Aqwesome -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    

    <!--Oswald Light Font -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans|Merriweather" rel="stylesheet">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?= $base_url ?>css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="<?= $base_url ?>css/style.css?v=1.4" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?= $base_url ?>js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-97644153-1', 'auto');
      ga('send', 'pageview');

    </script>
    -->
  </head>
  <a href="#" class="back-to-top">Back to Top</a>
  <body>

    <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-fixed-top navbar-site">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?= $base_url ?>"><img src="<?= $base_url ?>imgs/logo.png" alt="Puntoaparte"></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li<?php if($pageName == 'proyectos'){ ?> class="active"<?php } ?>><a class="nav-link" href="<?= $base_url.'proyectos' ?>">PROYECTOS</a></li>
            <li<?php if($pageName == 'noticias'){ ?> class="active"<?php } ?>><a class="nav-link" href="<?= $base_url.'noticias' ?>">NOTICIAS</a></li>
            <li<?php if($pageName == 'nosotros' || $pageName == 'equipo'){ ?> class="active"<?php } ?>><a class="nav-link" href="<?= $base_url.'nosotros' ?>">NOSOTROS</a></li>
            <li<?php if($pageName == 'contacto'){ ?> class="active"<?php } ?>><a class="nav-link" href="<?= $base_url.'contacto' ?>">CONTACTO</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>