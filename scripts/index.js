var folder = '../imgs/proyectos/';
var fs = require('fs');

function addFiles(path,files){
	let files_directory = fs.readdirSync(path);
	files_directory.forEach(file => {
	let file_stat = fs.statSync(path+file);
	if(file_stat.isFile()){
		files.push(file);
	}
	else if(file_stat.isDirectory()){
		let nFiles = addFiles(path+file+"/",[]);
		files.push({name:file,files:nFiles});
	}
	});
	return files;
}

console.log(addFiles(folder,[]));