function on_submit_form(){
	var dic = $('#form_contacto').serializeArray();
	var form = {};
    $.map(dic, function(n, i){
        form[n['name']] = n['value'];
    });
	console.log(form);
	if(form['form-nombre'] && form['form-mail'] && form['form-checked']){
		console.log('Formulario completo')
		$( "#form_mensaje" ).text( "Enviando" ).show();
		$.ajax({
			method:'POST',
		  	url: "sendemail.php",
		  	data: form
		}).done(function(res) {
			console.log('res: ',res)
		  	$( "#form_mensaje" ).text( "Formulario enviado!" ).show().fadeOut( 3000 ,function(){
				$( "#form_mensaje" ).text( "" ).show();
			});
			$( '#form_contacto' ).each(function(){
			    this.reset();
			});
		});
	}
	else{
		console.log('Formulario incompleto')
		$( "#form_mensaje" ).text( "Diligencia todos los campos" ).show();
	}
}