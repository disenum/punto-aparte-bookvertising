
<?php

    define('BASE_PATH', '/routing_test');

    require_once 'inc/functions/routing.php';

    $base_url = isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off' ? 'https' : 'http';
    $base_url .= '://'. $_SERVER['HTTP_HOST'];
    $base_url .= str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']);

    $pageName = explode("?",getPageName(0))[0];
    $subpageName = getPageName(1);
?>

<?php include 'inc/components/header.php'; ?>

<?php
    if($pageName == "proyectos"){
        require_once 'inc/model/proyectos.php';

        if($subpageName){
            $file = "inc/content/content-proyectos-interna.php";
        }
        else{
            $file = 'inc/content/content-' . $pageName . '.php';
        }
        include $file;
    }
    elseif($pageName == "noticias"){
        require_once 'inc/model/noticias.php';

        if($subpageName){
            $file = "inc/content/content-noticias-interna.php";
        }
        else{
            $file = 'inc/content/content-' . $pageName . '.php';
        }
        include $file;

    }
    elseif (file_exists($file = 'inc/content/content-' . $pageName . '.php'))
    {
        include $file;
    }
    else
    {
        echo 'not found'.$pageName;
    }

?>

<?php

    if (file_exists($file = 'inc/components/footer-' . $pageName . '.php'))
    {
        include $file;
    }
    else
    {
        include 'inc/components/footer.php';
    }

?>

